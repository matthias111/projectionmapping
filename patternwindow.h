#ifndef PATTERNWINDOW_H
#define PATTERNWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QTimer>
#include <QKeyEvent>
#include <opencv2/core.hpp>
#include <iostream>

#include "opencv/binarycode.h"

class PatternWindow : public QWidget
{
    Q_OBJECT

public:
    PatternWindow(QRect _screen);
    ~PatternWindow();

    void setTimer(int time_ms);
    int getNumberOfPatterns();
    QRect getScreen();

protected:
    // Key
    void keyPressEvent(QKeyEvent  *event);

public slots:
    void start();
    void stop();
    void pause();
    void nextPattern();
    void previousPattern();

signals:
    void currentPattern(int num);
    void isStopt();

private:
    // Screen
    QRect screen;

    // Patterns
    std::vector<QPixmap> generatetPattern;
    int currentPatternID;

    // Timer
    int waitTime;
    QTimer *timer;
    bool timerPaused;


    // GUI
    QGridLayout *viewLayout;
    QLabel *imageLabel;

    // Update Pattern
    void updatePattern();
};

#endif // PATTERNWINDOW_H
