#include "previewwindow.h"


PreviewWindow::PreviewWindow()
{
    setWindowTitle(tr("Preview"));

    // Set up ImageLable
    imageLabel = new QLabel();
    imageLabel->setScaledContents(true);

    // Set up ScrollArea
    scrollArea = new previewScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setAlignment(Qt::AlignCenter);
    scrollArea->setWidget(imageLabel);

    // Set up Layout
    viewLayout = new QGridLayout;
    viewLayout->addWidget(scrollArea);
    this->setLayout(viewLayout);

    // Window
    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    this->setAttribute( Qt::WA_QuitOnClose, false );

    // Signal and Slots
    connect(scrollArea,SIGNAL(mouseScroll(int)),this, SLOT(zoom(int)));
}

PreviewWindow::~PreviewWindow()
{
    this->close();
}

void PreviewWindow::showImagePreview(QPixmap image)
{
    // Set new Position if Window is closed
    if (!this->isVisible())
        this->setGeometry(pos.x(), pos.y(), 600,450);

    // Set Image and show
    imageLabel->setPixmap(image);
    imageLabel->resize(image.size());
    scaleFactor = 1;
    this->show();
    this->fitWindow();
}

void PreviewWindow::zoom(int v)
{
    if(v>0)
        scaleImage(1.1);
    else
        scaleImage(0.9);
}

void PreviewWindow::scaleImage(double factor)
{
    scaleFactor *= factor;
    imageLabel->resize(scaleFactor * imageLabel->pixmap()->size());
}

void PreviewWindow::fitWindow()
{
    scaleImage(double(scrollArea->width())/double(imageLabel->width()));
}
