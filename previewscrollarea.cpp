#include "previewscrollarea.h"

previewScrollArea::previewScrollArea()
{
}

void previewScrollArea::mouseMoveEvent(QMouseEvent *event)
{
    if (event->DragMove && event->buttons() == Qt::LeftButton)
    {
        this->verticalScrollBar()->setValue(this->verticalScrollBar()->value() - (event->y() - lastPosition.y()));
        this->horizontalScrollBar()->setValue(this->horizontalScrollBar()->value() - (event->x() - lastPosition.x()));
    }

    // Save last Mouse Possition
    lastPosition = QPoint(event->pos());
}

void previewScrollArea::wheelEvent(QWheelEvent *event)
{
    emit mouseScroll(event->delta());
}

void previewScrollArea::mousePressEvent(QMouseEvent *event)
{
    // Save first Mouse Possition
    lastPosition = event->pos();
}

