QT       += concurrent widgets core gui

TARGET = ProjectionMapping

# Version Number
VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_BUILD = 2
DEFINES += "VERSION_MAJOR=$$VERSION_MAJOR"\
       "VERSION_MINOR=$$VERSION_MINOR"\
       "VERSION_BUILD=$$VERSION_BUILD"
VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

# OpenCV
INCLUDEPATH += "C:/opencv411/install/include"

CONFIG( debug, debug|release ) {
LIBS += -L"C:/opencv411/install/x64/vc15/lib" \
     -lopencv_core411d \
     -lopencv_imgcodecs411d \
     -lopencv_imgproc411d
}
else {
LIBS += -L"C:/opencv411/install/x64/vc15/lib" \
     -lopencv_core411 \
     -lopencv_imgcodecs411 \
     -lopencv_imgproc411
}

SOURCES += \
    opencv/binarycode.cpp \
    main.cpp \
    mainwindow.cpp \
    binarycodepattern.cpp \
    previewscrollarea.cpp \
    supportFunktions.cpp \
    patternwindow.cpp \
    previewwindow.cpp

HEADERS += \
    opencv/binarycode.h \
    mainwindow.h \
    binarycodepattern.h \
    previewscrollarea.h \
    supportFunktions.h \
    patternwindow.h \
    previewwindow.h
