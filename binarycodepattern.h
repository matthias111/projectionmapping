#ifndef BINARYCODEPATTERN_H
#define BINARYCODEPATTERN_H

#include <iostream>
#include <sstream>
#include <iomanip>
#include <QtConcurrent>
#include <QThread>
#include <QPixmap>
#include <QObject>
#include <QDebug>
#include <QTime>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

#include "supportFunktions.h"
#include "opencv/binarycode.h"

class binarycodepattern : public QObject
{
    Q_OBJECT

public:
    binarycodepattern(int width, int heigth, QObject *parent = nullptr);
    ~binarycodepattern();

    // Prozessing
    bool decode();
    bool interpolate();
    bool resize();
    bool saveResult(QString url, bool ouputAllData);
    bool loadImages(QStringList urls);

    // Set
    int setBlackThreshold(int threshold);
    int setWhiteThreshold(int threshold);
    int setInterpolationWindowSize(int windowSize);
    double setInterpolationThreshold(double threshold);
    int setInterpolationIterations(int itterations);
    double setResizeFactor(double factor);

    // Get
    int getNumberOfPatternImages() {return int(numberOfPatternImages);}
    int getNumberOfLoadedPatternImages() {return int(numberOfLoadetPatternImages);}
    cv::Size getBeamerSize(){return cv::Size(beamerWidth, beamerHeight);}
    bool isWhiteImageLoaded(){return whiteImage.data;}
    bool isBlackImageLoaded(){return blackImage.data;}
    bool isColorImageLoaded(){return colorImage.data;}
    bool isDataLoaded();

public slots:
    void imageLoadingSaveImage(int num);
    void imageLoadingFinished();
    void decodingTileFinished();
    void decodingFinished();
    void interpolationFinished();
    void resizingFinished();

signals:
    void dataLoadingUpdate();
    void reportInfo(QString info);
    void status(QString status);
    void sendPreview(QPixmap image);

private:
    // Prozessing results
    cv::Mat resultImage, maskResult, resultImage_t, interpolatedImage, resizedImage, interpolatedMask, resizedMask;
    cv::Mat map_X, map_Y, map_X_interpolated, map_Y_interpolated;
    cv::Mat shadowMask;

    // Images
    std::vector<cv::Mat> captured_images;
    cv::Mat whiteImage, blackImage, colorImage;

    // Image and Beamer Size
    int imageWidth, imageHeigth, beamerWidth, beamerHeight;

    // Thresholds
    int blackThreshold, whiteThreshold;

    // Interpolatio settings
    int interpolationWindowSize;
    double interpolationThreshold;
    int interpolationIterations;
    double resizeFaktor;

    // Decoding
    int decodedTiles, numberOfTiles;

    // Pattern Information
    size_t numberOfPatternImages, numberOfLoadetPatternImages, numberOfExtraImages;
    size_t numOfRowImgs, numOfColImgs;

    // QFutureWatcher
    QFutureWatcher<cv::Mat> *imageLoadingWatcher;
    QFutureWatcher<decodingResult> *decodingWatcher;
    QFutureWatcher<interpolationResult> *interpolationWatcher;
    QFutureWatcher<cv::Mat> *resizeWatcher;

    // Timer
    QTime timer;

    // Compleate Output
    bool outputAllData;

    void computeShadowMasks(); // Computes the shadows occlusion where we cannot reconstruct the model
    void initFutureWatcher();
    void clearData();
};

#endif // BINARYCODEPATTERN_H
