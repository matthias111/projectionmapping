#include "mainwindow.h"

#include <functional>

MainWindow::MainWindow()
{
    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    // Create Contens
    createCapPatternTab();
    createDecPatternTab();
    createAboutTab();
    createProtocolTextArea();

    // Create TabWidget and add Tabs
    tabWidget = new QTabWidget;
    tabWidget->addTab(capPatternTab, tr("Capture Pattern"));
    tabWidget->addTab(decPatternTab, tr("Decode Pattern"));
    tabWidget->addTab(aboutTab, tr("About"));

    // Create Layout
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tabWidget);
    mainLayout->addWidget(protocollTextEdit);

    // Set layout
    widget->setLayout(mainLayout);

    // Set Window Title
    setWindowTitle(QString("ProjectionMapping %1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_BUILD));

    // Init Status Bar
    statusBar()->showMessage(QString(""));

    // Preview Window
    previewWindow = new PreviewWindow();

    // Signals
    connect(startProjectPatternButton,SIGNAL(clicked(bool)),this, SLOT(onClickProject()));
    connect(identifyButton,SIGNAL(clicked(bool)),this, SLOT(onClickIdentify()));
    connect(beamerSelectionComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onSelectBeamer()));
    connect(selectImageFolderbutton, SIGNAL(clicked(bool)), this, SLOT(onClickOpenImages()));
    connect(selectOutputFolderbutton, SIGNAL(clicked(bool)), this, SLOT(onClickSelectOutputFolder()));
    connect(decodePatternButton, SIGNAL(clicked(bool)), this, SLOT(onClickDecode()));
    connect(interpolateButton, SIGNAL(clicked(bool)), this, SLOT(onClickInterpolate()));
    connect(saveButton, SIGNAL(clicked(bool)), this, SLOT(onClickSave()));
    connect(interpolationWindowSizeLineEdit, SIGNAL(editingFinished()),this, SLOT(onChangeIntWinSize()));
    connect(interpolationThresholdLineEdit, SIGNAL(editingFinished()),this, SLOT(onChangeIntThresho()));
    connect(blackThresholdLineEdit, SIGNAL(editingFinished()),this, SLOT(onChangeThresholdMask()));
    connect(whiteThresholdLineEdit, SIGNAL(editingFinished()),this, SLOT(onChangeThresholdDecode()));
    connect(interpolationIterationsLineEdit, SIGNAL(editingFinished()), this, SLOT(onChangeIterations()));
    connect(resizeFactorLineEdit, SIGNAL(editingFinished()), this, SLOT(onChangeResizeFactor()));
    connect(beamerHeightLineEdit, SIGNAL(editingFinished()), this, SLOT(onSelectBeamer()));
    connect(beamerWidthLineEdit, SIGNAL(editingFinished()), this, SLOT(onSelectBeamer()));
}

MainWindow::~MainWindow()
{
    binarycodepatternObject->~binarycodepattern();
    previewWindow->~PreviewWindow();
}

void MainWindow::setDesktopWidget(QDesktopWidget *_desktopWidget)
{
    desktopWidget = _desktopWidget;
    connect(desktopWidget, SIGNAL(screenCountChanged(int)), this, SLOT(createBeameraList()));
    createBeameraList();
}

void MainWindow::moveEvent(QMoveEvent *event)
{
    previewWindow->pos = QPoint(event->pos().x() + this->width() + 5,event->pos().y());
}

void MainWindow::createCapPatternTab()
{
    capPatternTab = new QWidget(this);
    QVBoxLayout *layout = new QVBoxLayout;

    // Beamera Selection
    QGroupBox *beamerBox  = new QGroupBox(tr("Projector"));
    QGridLayout *beamerSelLayout = new QGridLayout;
    beamerSelLayout->setColumnStretch(0,1);
    beamerSelLayout->setColumnStretch(1,1);
    beamerSelectionComboBox = new QComboBox();
    identifyButton = new QPushButton(tr("Identify"));
    beamerSelLayout->addWidget(beamerSelectionComboBox,0,0,1,1);
    beamerSelLayout->addWidget(identifyButton,0,1,1,1);
    labelOther = new QLabel(tr("Other (Width/Height):"));
    beamerSelLayout->addWidget(labelOther,1,0,1,2);
    beamerWidthLineEdit = new QLineEdit();
    beamerWidthLineEdit->setAlignment(Qt::AlignRight);
    beamerSelLayout->addWidget(beamerWidthLineEdit,2,0,1,1);
    beamerHeightLineEdit = new QLineEdit();
    beamerHeightLineEdit->setAlignment(Qt::AlignRight);
    beamerSelLayout->addWidget(beamerHeightLineEdit,2,1,1,1);
    beamerBox->setLayout(beamerSelLayout);
    layout->addWidget(beamerBox);

    // Capture Selection
    QGroupBox *captureBox  = new QGroupBox(tr("Capture"));
    QGridLayout *cameraSelLayout = new QGridLayout;
    cameraSelLayout->setColumnStretch(0,1);
    cameraSelLayout->setColumnStretch(1,1);

    // Delay Time
    waitTimeLineEdit = new QLineEdit(tr("500"));
    waitTimeLineEdit->setAlignment(Qt::AlignRight);
    labelPatternDelay = new QLabel(tr("Pattern Deley (ms):"));
    cameraSelLayout->addWidget(labelPatternDelay,1,0,1,1);
    cameraSelLayout->addWidget(waitTimeLineEdit,1,1,1,1);

    // Progress Bar
    progressBar = new QProgressBar;
    progressBar->setValue(0);
    progressBar->setMinimum(0);
    progressBar->setAlignment(Qt::AlignCenter);
    progressBar->setFormat(QString("%v/%m"));
    cameraSelLayout->addWidget(progressBar,2,0,1,2);

    // Button Generate Pattern
    startProjectPatternButton = new QPushButton(tr("Project"));
    startProjectPatternButton->setStyleSheet("font-weight: bold");
    cameraSelLayout->addWidget(startProjectPatternButton,3,0,1,1);
    stopProjectPatternButton = new QPushButton(tr("Stop"));
    stopProjectPatternButton->setDisabled(true);
    cameraSelLayout->addWidget(stopProjectPatternButton, 3,1,1,1);

    // Load Images
    cameraSelLayout->addWidget(new QLabel(tr("Images:")),4,0,1,1);
    selectImageFolderbutton = new QPushButton(tr("Open..."));
    cameraSelLayout->addWidget(selectImageFolderbutton,4,1,1,1);
    captureBox->setLayout(cameraSelLayout);
    layout->addWidget(captureBox);

    // Info Area
    QGroupBox *infoBox = new QGroupBox(tr("Info Images"));
    QGridLayout *infoLayout = new QGridLayout; // 2 Collums
    infoLayout->setColumnStretch(0,1);
    infoLayout->setColumnStretch(1,1);
    infoLayout->addWidget(new QLabel(tr("Projector Size:")),0,0,1,1);
    infoLayout->addWidget(new QLabel(tr("Patterns:")),1,0,1,1);
    infoLayout->addWidget(new QLabel(tr("White Image:")),2,0,1,1);
    infoLayout->addWidget(new QLabel(tr("Black Image:")),3,0,1,1);
    infoLayout->addWidget(new QLabel(tr("Color Image:")),4,0,1,1);
    labelBeamerSizeInfo = new QLabel(tr("-x-"));
    labelPatternInfo = new QLabel(tr("-/-"));
    labelWhiteImageInfo  = new QLabel(tr("-"));
    labelBlackImageInfo = new QLabel(tr("-"));
    labelColorImageInfo = new QLabel(tr("-"));
    infoLayout->addWidget(labelBeamerSizeInfo,0,1,1,1);
    infoLayout->addWidget(labelPatternInfo,1,1,1,1);
    infoLayout->addWidget(labelWhiteImageInfo,2,1,1,1);
    infoLayout->addWidget(labelBlackImageInfo,3,1,1,1);
    infoLayout->addWidget(labelColorImageInfo,4,1,1,1);
    infoBox->setLayout(infoLayout);
    layout->addWidget(infoBox);

    // Add Layout of Capture Tab to Tab
    capPatternTab->setLayout(layout);
}

void MainWindow::createDecPatternTab()
{
    decPatternTab = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout;

    // Decoding
    QGroupBox *decodingBox  = new QGroupBox(tr("Decoding"));
    QGridLayout *decodingLayout = new QGridLayout;
    decodingLayout->setColumnStretch(0,1);
    decodingLayout->setColumnStretch(1,1);
    decodingLayout->addWidget(new QLabel(tr("Threshold Mask [0-255]:")),0,0,1,1);
    blackThresholdLineEdit = new QLineEdit(tr("150"));
    blackThresholdLineEdit->setAlignment(Qt::AlignRight);
    decodingLayout->addWidget(blackThresholdLineEdit,0,1,1,1);
    decodingLayout->addWidget(new QLabel(tr("Threshold Decoding [0-255]:")),1,0,1,1);
    whiteThresholdLineEdit = new QLineEdit(tr("3"));
    whiteThresholdLineEdit->setAlignment(Qt::AlignRight);
    decodingLayout->addWidget(whiteThresholdLineEdit,1,1,1,1);
    decodePatternButton = new QPushButton(tr("Decode"));
    decodePatternButton->setStyleSheet("font-weight: bold");
    decodingLayout->addWidget(decodePatternButton,2,1,1,1);
    decodingBox->setLayout(decodingLayout);
    layout->addWidget(decodingBox);

    // Post Prozessing
    QGroupBox *interpolationBox  = new QGroupBox(tr("Post-processing"));
    QGridLayout *interpolationLayout = new QGridLayout;
    interpolationLayout->setColumnStretch(0,1);
    interpolationLayout->setColumnStretch(1,1);
    interpolationLayout->addWidget(new QLabel(tr("Window [0-255]:")),0,0,1,2);
    interpolationWindowSizeLineEdit = new QLineEdit(tr("7"));
    interpolationWindowSizeLineEdit->setAlignment(Qt::AlignRight);
    interpolationLayout->addWidget(interpolationWindowSizeLineEdit,0,1,1,1);
    interpolationLayout->addWidget(new QLabel(tr("Threshold [0-1]:")),1,0,1,2);
    interpolationThresholdLineEdit = new QLineEdit(tr("0.5"));
    interpolationThresholdLineEdit->setAlignment(Qt::AlignRight);
    interpolationLayout->addWidget(interpolationThresholdLineEdit,1,1,1,1);
    interpolationLayout->addWidget(new QLabel(tr("Iterations [>0]:")),2,0,1,2);
    interpolationIterationsLineEdit = new QLineEdit(tr("2"));
    interpolationIterationsLineEdit->setAlignment(Qt::AlignRight);
    interpolationLayout->addWidget(interpolationIterationsLineEdit,2,1,1,1);
    interpolationLayout->addWidget(new QLabel(tr("Resize Factor [>0]:")),3,0,1,2);
    resizeFactorLineEdit = new QLineEdit(tr("2"));
    resizeFactorLineEdit->setAlignment(Qt::AlignRight);
    interpolationLayout->addWidget(resizeFactorLineEdit,3,1,1,1);
    interpolateButton = new QPushButton(tr("Prozess"));
    interpolateButton->setStyleSheet("font-weight: bold");
    interpolationLayout->addWidget(interpolateButton,4,1,1,1);
    interpolationBox->setLayout(interpolationLayout);
    layout->addWidget(interpolationBox);

    // Output Folder
    QGroupBox *outputBox  = new QGroupBox(tr("Output"));
    QGridLayout *outputLayout = new QGridLayout;
    outputLayout->setColumnStretch(0,1);
    outputLayout->setColumnStretch(1,1);
    oututFolderLineEdit = new QLineEdit();
    oututFolderLineEdit->setReadOnly(true);
    outputLayout->addWidget(oututFolderLineEdit,0,0,1,1);
    selectOutputFolderbutton = new QPushButton(tr("Browse..."));
    outputLayout->addWidget(selectOutputFolderbutton,0,1,1,1);
    outputDataCheckBox = new QCheckBox(tr("Output all data"));
    outputLayout->addWidget(outputDataCheckBox, 1,0,1,1);
    saveButton = new QPushButton(tr("Save"));
    saveButton->setStyleSheet("font-weight: bold");
    outputLayout->addWidget(saveButton,1,1,1,1);
    outputBox->setLayout(outputLayout);
    layout->addWidget(outputBox);

    // Add Layout
    decPatternTab->setLayout(layout);
}

void MainWindow::createAboutTab()
{
    aboutTab = new QWidget();
    QVBoxLayout *layout = new QVBoxLayout;
    QTextBrowser *text = new QTextBrowser;
    text->setReadOnly(true);
    text->setOpenExternalLinks(true);
    text->setHtml("<h1>ProjectionMapping</h1>For current version and source code see: <a href='https://gitlab.com/matthias111/projectionmapping'>https://gitlab.com/matthias111/projectionmapping</a><h2>Purpose</h2>This program will create a virtual image from the view of the projector using a camera. It will work best on bright, non-reflective surfaces. This image can than be used to create masks for the projector.<h2>Quick Usage</h2><b>Configuration:</b> Projector and camera should have a similar field of view. The camera has to be on a tripod with fixed image parameters (ISO, exposure time, aperture and focus). Connect projector to PC and select it from the drop down menu. <p><b>Capture Pattern:</b> Start projection by pressing [Project] and use the Camera to capture every pattern image. Capture one optional color image. This additional image will be used for the mapping in to the projector view, otherwise the black image will be used. Load all images with [Open...].<p><b>Decode Pattern:</b>Press Decode to see preview. Change settings to improve result. Post Processing will interpolate small missing areas and create a high resolution image (e.g. twice the projector resolution). Save result by choosing output folder [Browse...] and pressing [Save].<h2>License</h2>Copyright © 2019 Matthias Hardner <p>This program is free software: you can redistribute it and/or modifyit under the terms of the GNU General Public License as published bythe Free Software Foundation, either version 3 of the License, or(at your option) any later version. <p>This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty ofMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See theGNU General Public License for more details. <p>You should have received a copy of the GNU General Public Licensealong with this program.  <a href='https://www.gnu.org/licenses/'>https://www.gnu.org/licenses/</a>.<p>--Note: The code for creating and decoding the patterns is copied from OpenCV and licensed under 3-clause BSD. See Modul Structured Light. <h2>Third party</h2>OpenCV, <a href='https://opencv.org'>opencv.org</a>, 3-clause BSD License<p>Qt, <a href='https://www.qt.io/'>www.qt.io</a><p>");
    layout->addWidget(text);

    // Add Layout
    aboutTab->setLayout(layout);
}


void MainWindow::createBeameraList()
{
    // Clear List
    screens.clear();
    beamerSelectionComboBox->clear();

    // Read available Screens
    for(int i = 0; i < desktopWidget->screenCount(); i++) screens.push_back(desktopWidget->screenGeometry(i));

    // Create Combobox list
    QString p = QString("Info: %1 Screen(s) detected: ").arg(screens.size());
    for(auto const &s : screens){
        QString b = QString("%1x%2 ").arg(s.width()).arg(s.height());
        p += b;
        beamerSelectionComboBox->addItem(b);
    }
    protocollTextEdit->append(p);
    beamerSelectionComboBox->addItem(QString("<other>"));
    beamerSelectionComboBox->setCurrentIndex(0);
    onSelectBeamer();
}


void MainWindow::createProtocolTextArea()
{
    protocollTextEdit  = new QTextEdit(this);
    protocollTextEdit->setReadOnly(true);
}

void MainWindow::onClickProject()
{
    // Get Selected Screen Geometry and set up Pattern Window
    QRect screen = screens[beamerSelectionComboBox->currentIndex()];
    patternWindow  = new PatternWindow(screen);
    patternWindow->setTimer(waitTimeLineEdit->text().toInt());
    connect(stopProjectPatternButton, SIGNAL(clicked(bool)), patternWindow, SLOT(stop()));
    connect(patternWindow, SIGNAL(isStopt()), this, SLOT(stopProjection()));

    // Enable Stop push button
    stopProjectPatternButton->setDisabled(false);
    startProjectPatternButton->setDisabled(true);

    // Set up Progressbar
    progressBar->setDisabled(false);
    connect(patternWindow, SIGNAL(currentPattern(int)), progressBar, SLOT(setValue(int)));

    // Start
    patternWindow->show();
    patternWindow->showFullScreen();
    patternWindow->start();
}

void MainWindow::stopProjection()
{
    stopProjectPatternButton->setDisabled(true);
    startProjectPatternButton->setDisabled(false);
    progressBar->setValue(0);
    progressBar->setDisabled(true);
}

void MainWindow::onClickIdentify()
{
    identifyScreenWidget = new QWidget();
    identifyScreenWidget->move(screens[beamerSelectionComboBox->currentIndex()].center());

    // Add Label with Screen Size
    QGridLayout *layout = new QGridLayout;
    QLabel *beameraLable = new QLabel(beamerSelectionComboBox->currentText());
    QFont f("Arial", 60, QFont::Bold);
    beameraLable->setFont(f);
    layout->addWidget(beameraLable,0,0,1,1,Qt::AlignHCenter);
    identifyScreenWidget->setLayout(layout);

    // Show Widget
    identifyScreenWidget->show();
    identifyScreenWidget->showFullScreen();

    // Timer to close Window
    identifyTimer = new QTimer(this);
    identifyTimer->start(1500);
    connect(identifyTimer, SIGNAL(timeout()),identifyScreenWidget,SLOT(close()));
}

void MainWindow::onSelectBeamer()
{
    // Check if at least 2 items in list (1 screen + <other)
    if (beamerSelectionComboBox->count() < 2) return;

    // Check if <other> is selected (last Item)
    bool other = beamerSelectionComboBox->currentIndex()==beamerSelectionComboBox->count()-1;

    // Set Beamer Size
    int beamerWidth = 0, beamerHeigth = 0;
    if (other){
        // Get Custom Values
        beamerWidth = beamerWidthLineEdit->text().toInt();
        beamerHeigth = beamerHeightLineEdit->text().toInt();
    }
    else {
        // If conneted Beamer is uesd
        beamerWidth = screens[beamerSelectionComboBox->currentIndex()].width();
        beamerHeigth = screens[beamerSelectionComboBox->currentIndex()].height();
    }

    // Init patternObject
    binarycodepatternObject = new binarycodepattern(beamerWidth, beamerHeigth);

    // Set Up Signals
    connect(binarycodepatternObject, SIGNAL(dataLoadingUpdate()), this, SLOT(updateInfoBox()));
    connect(binarycodepatternObject, SIGNAL(reportInfo(QString)), this, SLOT(printProtocol(QString)));
    connect(binarycodepatternObject, SIGNAL(status(QString)), this, SLOT(showStatus(QString)));
    connect(binarycodepatternObject, SIGNAL(sendPreview(QPixmap)), previewWindow, SLOT(showImagePreview(QPixmap)));

    // Disable/Enable Objects
    waitTimeLineEdit->setDisabled(other);
    startProjectPatternButton->setDisabled(other);
    identifyButton->setDisabled(other);
    labelPatternDelay->setDisabled(other);
    beamerHeightLineEdit->setDisabled(!other);
    beamerWidthLineEdit->setDisabled(!other);
    labelOther->setDisabled(!other);
    progressBar->setDisabled(other);
    progressBar->setMaximum(binarycodepatternObject->getNumberOfPatternImages()+2);

    // Display Screen Information
    updateInfoBox();
}

void MainWindow::updateInfoBox()
{
    if (binarycodepatternObject) {
        cv::Size beamerSize = binarycodepatternObject->getBeamerSize();

        labelBeamerSizeInfo->setText(QString("%1x%2").arg(beamerSize.width).arg(beamerSize.height));

        int nNPattern = binarycodepatternObject->getNumberOfPatternImages();
        int nLPattern = binarycodepatternObject->getNumberOfLoadedPatternImages();
        labelPatternInfo->setText(QString("%1/%2").arg(nLPattern).arg(nNPattern));

        if (binarycodepatternObject->isWhiteImageLoaded())
            labelWhiteImageInfo->setText(tr("OK"));

        if (binarycodepatternObject->isBlackImageLoaded())
            labelBlackImageInfo->setText(tr("OK"));

        if (binarycodepatternObject->isColorImageLoaded())
            labelColorImageInfo->setText(tr("OK"));
    }
    else {
        labelPatternInfo = new QLabel(tr("-/-"));
        labelWhiteImageInfo  = new QLabel(tr("-"));
        labelBlackImageInfo = new QLabel(tr("-"));
        labelColorImageInfo = new QLabel(tr("-"));
    }
}

void MainWindow::onClickSelectOutputFolder()
{
    oututFolderLineEdit->setText(QFileDialog::getExistingDirectory(this,tr("Select Output Folder")));
}

void MainWindow::onClickInterpolate()
{
    binarycodepatternObject->interpolate();
}

void MainWindow::onClickSave()
{
    binarycodepatternObject->saveResult(oututFolderLineEdit->text(), outputDataCheckBox->isChecked());
}

void MainWindow::onClickOpenImages()
{
    // Get Image URLs
    QStringList selectedImageURLs = QFileDialog::getOpenFileNames(this,tr("Bilder öffnen"),QString(),tr("Bilddateien (*.jpg *.png)"));
    binarycodepatternObject->loadImages(selectedImageURLs);
}

void MainWindow::onChangeIntWinSize()
{
    int newValue = binarycodepatternObject->setInterpolationWindowSize(interpolationWindowSizeLineEdit->text().toInt());
    interpolationWindowSizeLineEdit->setText(QString("%1").arg(newValue));
}

void MainWindow::onChangeIntThresho()
{
    double newValue = binarycodepatternObject->setInterpolationThreshold(interpolationThresholdLineEdit->text().toDouble());
    interpolationThresholdLineEdit->setText(QString("%1").arg(newValue));
}

void MainWindow::onChangeThresholdMask()
{
    int newValue = binarycodepatternObject->setBlackThreshold(blackThresholdLineEdit->text().toInt());
    blackThresholdLineEdit->setText(QString("%1").arg(newValue));
}

void MainWindow::onChangeThresholdDecode()
{
    int newValue = binarycodepatternObject->setWhiteThreshold(whiteThresholdLineEdit->text().toInt());
    whiteThresholdLineEdit->setText(QString("%1").arg(newValue));
}

void MainWindow::onChangeIterations()
{
    int newValue = binarycodepatternObject->setInterpolationIterations(interpolationIterationsLineEdit->text().toInt());
    interpolationIterationsLineEdit->setText(QString("%1").arg(newValue));
}

void MainWindow::onChangeResizeFactor()
{
    double newValue = binarycodepatternObject->setResizeFactor(resizeFactorLineEdit->text().toDouble());
    resizeFactorLineEdit->setText(QString("%1").arg(newValue));
}

