# ProjectionMapping

Software for simple projection mapping using coded light method.

More Information see: [Manual.pdf](Manual.pdf)

Windows Binary: [projectionmapping_1.0.2.zip](https://dl.dropboxusercontent.com/s/be27tvy65fmjby9/ProjectionMapping_1.0.2.zip)
