#ifndef PREVIEWWINDOW_H
#define PREVIEWWINDOW_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QGridLayout>
#include <QMouseEvent>
#include <QWheelEvent>

#include <iostream>

#include "previewscrollarea.h"

class PreviewWindow : public QWidget
{
    Q_OBJECT

public:
    PreviewWindow();
    ~PreviewWindow();

    QPoint pos;

public slots:
    void showImagePreview(QPixmap image);
    void zoom(int v);

private:
    // GUI
    QGridLayout *viewLayout;
    QLabel *imageLabel;
    previewScrollArea *scrollArea;

    double scaleFactor;
    void scaleImage(double factor);
    void fitWindow();
};

#endif // PREVIEWWINDOW_H
