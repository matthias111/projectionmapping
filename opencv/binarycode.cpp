// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.


//                           License Agreement
//                For Open Source Computer Vision Library
//                        (3-clause BSD License)

// Copyright (C) 2000-2019, Intel Corporation, all rights reserved.
// Copyright (C) 2009-2011, Willow Garage Inc., all rights reserved.
// Copyright (C) 2009-2016, NVIDIA Corporation, all rights reserved.
// Copyright (C) 2010-2013, Advanced Micro Devices, Inc., all rights reserved.
// Copyright (C) 2015-2016, OpenCV Foundation, all rights reserved.
// Copyright (C) 2015-2016, Itseez Inc., all rights reserved.
// Third party copyrights are property of their respective owners.

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

//   * Redistributions of source code must retain the above copyright notice,
//     this list of conditions and the following disclaimer.

//   * Redistributions in binary form must reproduce the above copyright notice,
//     this list of conditions and the following disclaimer in the documentation
//     and/or other materials provided with the distribution.

//   * Neither the names of the copyright holders nor the names of the contributors
//     may be used to endorse or promote products derived from this software
//     without specific prior written permission.

// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall copyright holders or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.

#include "binarycode.h"

namespace binarycode {

int code2number(std::vector<bool> &code)
{

    // const int pow2[] = {1,2,4,8,16,32,64,128,256,512,1024,2048,4096};

    int number = 0;
    bool tmp = code[0];

    if (tmp){
        // dec += pow2[gray.size()-1];
        number += pow(2.0f, int(code.size() - 1));
    }
    for (unsigned int i = 1; i < code.size(); i++)
    {
        tmp = tmp ^ code[i]; // XOR
        if (tmp){
            //dec += pow2[gray.size() - i -1];
            number += pow(2.0f, int(code.size() - i - 1));
        }
    }
    return number;

}

void decodePatterns(const decodingInformation &_info, const cv::Rect &tile, decodingResult &_result)
{
    // Create Object for returning the result
    _result.anzahl = cv::Mat(_info._beamerHeight, _info._beamerWidth, CV_8UC1, cv::Scalar(0));
    _result.map_X_summ = cv::Mat(_info._beamerHeight, _info._beamerWidth, CV_32FC1, cv::Scalar(0));
    _result.map_Y_summ = cv::Mat(_info._beamerHeight, _info._beamerWidth, CV_32FC1, cv::Scalar(0));

    std::vector<bool> grayCol, grayRow;
    grayCol.reserve(_info._numOfColImgs);
    grayRow.reserve(_info._numOfRowImgs);

    for (int i = tile.x; i < tile.x+tile.width; i++) // Itterate over Tile
    {
        for (int j = tile.y; j < tile.y+tile.height; j++)
        {
            // check if pixel in area
            if (_info._shadowMask.at<uchar>(j, i) != 255) continue;

            // Clear vector
            grayCol.clear();
            grayRow.clear();

            // calc projector pixel for each camera pixel
            bool error = false;

            // column images
            for (size_t count = 0; count < _info._numOfColImgs && !error; count++)
            {
                // get pixel and inverse
                uchar val1 = _info._captured_images[count * 2].at<uchar>(cv::Point(i, j));
                uchar val2 = _info._captured_images[count * 2 + 1].at<uchar>(cv::Point(i, j));

                if (abs(val1 - val2) < _info._whiteThreshold)
                    error = true;

                grayCol.push_back(val1 > val2);  // save as true/false
            }

            // row images
            for (size_t count = 0; count < _info._numOfRowImgs && !error; count++)
            {
                // get pixel and inverse
                uchar val1 = _info._captured_images[count * 2 + _info._numOfColImgs * 2].at<uchar>(cv::Point(i, j));
                uchar val2 = _info._captured_images[count * 2 + _info._numOfColImgs * 2 + 1].at<uchar>(cv::Point(i, j));

                if (abs(val1 - val2) < _info._whiteThreshold)
                    error = true;

                grayRow.push_back(val1 > val2);  // save as true/false
            }
            if (error) continue;

            // Convert code to projector pixel
            cv::Point projPixel;
            projPixel.x = code2number(grayCol);
            projPixel.y = code2number(grayRow);

            if ((projPixel.y >= _info._beamerHeight || projPixel.x >= _info._beamerWidth))
                continue;

            float &oldImageValue_x = _result.map_X_summ.at<float>(projPixel);
            float &oldImageValue_y = _result.map_Y_summ.at<float>(projPixel);
            oldImageValue_x += float(i);
            oldImageValue_y += float(j);

            // Add number of decoded Values (for taking the mean)
            uchar &oldNumberOfDecodedValues = _result.anzahl.at<uchar>(projPixel);
            oldNumberOfDecodedValues++;
        }
    }
}

unsigned int calcNumberOfPatternImages(unsigned int size)
{
    return int(ceil(log(double(size)) / log(2.0)));
}

void generatePatterns(int width, int heigth, std::vector<cv::Mat> &pattern_mat)
{
    // Calculating Pattern Information
    unsigned int numOfColImgs = calcNumberOfPatternImages(width);
    unsigned int numOfRowImgs = calcNumberOfPatternImages(heigth);
    unsigned int numberOfPatternImages = 2 * numOfColImgs + 2 * numOfRowImgs;

    // Generate empty Images
    for(unsigned int i = 0; i < numberOfPatternImages; i++ ) pattern_mat.push_back(cv::Mat( heigth, width, CV_8U ));

    uchar flag = 0;

    for( int j = 0; j < width; j++ )  // col loop
    {
        int rem = 0, num = j, prevRem = j % 2;

        for( size_t k = 0; k < numOfColImgs; k++ )  // images loop
        {
            num = num / 2;
            rem = num % 2;

            if( ( rem == 0 && prevRem == 1 ) || ( rem == 1 && prevRem == 0) )
            {
                flag = 1;
            }
            else
            {
                flag = 0;
            }

            for( int i = 0; i < heigth; i++ )  // rows loop
            {
                uchar pixel_color = ( uchar ) flag * 255;

                pattern_mat[2 * numOfColImgs - 2 * k - 2].at<uchar>( i, j ) = pixel_color;
                if( pixel_color > 0 )
                    pixel_color =  uchar(0) ;
                else
                    pixel_color =  uchar(255);
                pattern_mat[2 * numOfColImgs - 2 * k - 1].at<uchar>( i, j ) = pixel_color;  // inverse
            }

            prevRem = rem;
        }
    }

    for( int i = 0; i < heigth; i++ )  // rows loop
    {
        int rem = 0, num = i, prevRem = i % 2;

        for( size_t k = 0; k < numOfRowImgs; k++ )
        {
            num = num / 2;
            rem = num % 2;

            if( (rem == 0 && prevRem == 1) || (rem == 1 && prevRem == 0) )
                flag = 1;
            else
                flag = 0;

            for( int j = 0; j < width; j++ )
            {
                uchar pixel_color = uchar(flag * 255);
                pattern_mat[2 * numOfRowImgs - 2 * k + 2 * numOfColImgs - 2].at<uchar>( i, j ) = pixel_color;

                pixel_color = (pixel_color > 0) ? uchar(0) : uchar(255);
//                if( pixel_color > 0 ) pixel_color = uchar(0);
//                else pixel_color = uchar(255);

                pattern_mat[2 * numOfRowImgs - 2 * k + 2 * numOfColImgs - 1].at<uchar>( i, j ) = pixel_color;
            }

            prevRem = rem;
        }
    }

    // Black and White Image
    pattern_mat.push_back(cv::Mat( heigth, width, CV_8U, cv::Scalar(0)));
    pattern_mat.push_back(cv::Mat( heigth, width, CV_8U, cv::Scalar(255)));
}

void generatePatterns(int width, int heigth, std::vector<QPixmap> &generatetPattern)
{
    std::vector<cv::Mat> _patterns;
    generatePatterns(width, heigth, _patterns);

    // Convert Patterns to QPixmap
    for (auto p : _patterns){
        QVector<QRgb> colorTable;
        for (int i=0; i<256; i++) colorTable.push_back(qRgb(i,i,i));
        const uchar *qImageBuffer = (const uchar*)p.data;
        QImage img(qImageBuffer, p.cols, p.rows, p.step, QImage::Format_Indexed8);
        img.setColorTable(colorTable);
        generatetPattern.push_back(QPixmap::fromImage(img));
    }
}

}
