#ifndef PREVIEWSCROLLAREA_H
#define PREVIEWSCROLLAREA_H

#include <QObject>
#include <QScrollArea>
#include <QWheelEvent>
#include <QScrollArea>
#include <QScrollBar>

#include <iostream>

class previewScrollArea : public QScrollArea
{
    Q_OBJECT

public:
    previewScrollArea();

signals:
    void mouseScroll(int delta);

private:
    void mouseMoveEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);
    void mousePressEvent(QMouseEvent* event);

    QPoint lastPosition;

};

#endif // PREVIEWSCROLLAREA_H
