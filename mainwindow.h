#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtConcurrent>
#include <QGroupBox>
#include <QLabel>
#include <QComboBox>
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QDesktopWidget>
#include <QWindow>
#include <QFuture>
#include <QTimer>
#include <QFileDialog>
#include <QDebug>
#include <QStyle>
#include <QTextEdit>
#include <QtWidgets>
#include <QMainWindow>
#include <QTextBrowser>

#include <iostream>

#include <opencv2/core.hpp>

#include "binarycodepattern.h"
#include "patternwindow.h"
#include "previewwindow.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    ~MainWindow();

    void setDesktopWidget(QDesktopWidget *_desktopWidget);
    void moveEvent(QMoveEvent *event);

private:
    std::vector<QRect> screens;
    QDesktopWidget *desktopWidget;

    void createCapPatternTab();
    void createDecPatternTab();
    void createAboutTab();
    void createProtocolTextArea();

    // Tab
    QTabWidget *tabWidget;
    QWidget *capPatternTab;
    QWidget *decPatternTab;
    QWidget *aboutTab;

    // ComboBox
    QComboBox *beamerSelectionComboBox;

    // LineEdit
    QLineEdit *waitTimeLineEdit;
    QLineEdit *imagesFolderLineEdit;
    QLineEdit *blackThresholdLineEdit;
    QLineEdit *whiteThresholdLineEdit;
    QLineEdit *oututFolderLineEdit;
    QLineEdit *interpolationWindowSizeLineEdit;
    QLineEdit *interpolationThresholdLineEdit;
    QLineEdit *interpolationIterationsLineEdit;
    QLineEdit *resizeFactorLineEdit;
    QLineEdit *beamerWidthLineEdit;
    QLineEdit *beamerHeightLineEdit;

    // Button
    QPushButton *selectImageFolderbutton;
    QPushButton *selectOutputFolderbutton;
    QPushButton *startProjectPatternButton;
    QPushButton *stopProjectPatternButton;
    QPushButton *identifyButton;
    QPushButton *decodePatternButton;
    QPushButton *interpolateButton;
    QPushButton *saveButton;
    QPushButton *showMaskButton;

    // Check Box
    QCheckBox *outputDataCheckBox;

    // Protocoll View
    QTextEdit *protocollTextEdit;

    // Pattern View
    PatternWindow *patternWindow;
    QWidget *identifyScreenWidget;
    QTimer *identifyTimer;
    QProgressBar *progressBar;

    // Preview
    PreviewWindow *previewWindow;

    // binarypattern object
    binarycodepattern *binarycodepatternObject;

    // Labels
    QLabel *labelPatternDelay;
    QLabel *labelPatternInfo;
    QLabel *labelWhiteImageInfo;
    QLabel *labelBlackImageInfo;
    QLabel *labelColorImageInfo;
    QLabel *labelBeamerSizeInfo;
    QLabel *labelOther;


private slots:
    // On Click
    void onClickProject();
    void stopProjection();
    void onClickIdentify();
    void onSelectBeamer();
    void onClickSelectOutputFolder();
    void onClickDecode() {binarycodepatternObject->decode();}
    void onClickInterpolate();
    void onClickSave();
    void onClickOpenImages();

    // On Value change
    void onChangeIntWinSize();
    void onChangeIntThresho();
    void onChangeThresholdMask();
    void onChangeThresholdDecode();
    void onChangeIterations();
    void onChangeResizeFactor();
    void updateInfoBox();
    void createBeameraList();
    void printProtocol(QString text) {protocollTextEdit->append(text);}
    void showStatus(QString text) {statusBar()->showMessage(text, 1000);}
};

#endif // MAINWINDOW_H
