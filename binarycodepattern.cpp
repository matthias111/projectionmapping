#include "binarycodepattern.h"

binarycodepattern::binarycodepattern(int width, int heigth, QObject *parent) : QObject(parent)
{
    // Init Values
    numberOfLoadetPatternImages = 0;
    numberOfExtraImages = 0;
    decodedTiles = 0;
    numberOfTiles = 0;
    blackThreshold = 80;
    whiteThreshold = 3;
    interpolationWindowSize = 7;
    interpolationThreshold = 0.5;
    interpolationIterations = 1;
    resizeFaktor = 2;
    outputAllData = false;
    beamerWidth = 1920;
    beamerHeight = 1080;

    // Check if reasonable size
    if (width > 100 && heigth > 100){
        beamerWidth = width;
        beamerHeight = heigth;
    }

    // Calc pattern size
    numOfColImgs = binarycode::calcNumberOfPatternImages(beamerWidth);
    numOfRowImgs = binarycode::calcNumberOfPatternImages(beamerHeight);
    numberOfPatternImages = 2 * numOfColImgs + 2 * numOfRowImgs;

    clearData();
    initFutureWatcher();
}

binarycodepattern::~binarycodepattern()
{

    if (imageLoadingWatcher->isRunning()){
        imageLoadingWatcher->cancel();
        imageLoadingWatcher->waitForFinished();
    }

    if (decodingWatcher->isRunning()){
        decodingWatcher->cancel();
        decodingWatcher->waitForFinished();
    }

    if (interpolationWatcher->isRunning()){
        interpolationWatcher->cancel();
        interpolationWatcher->waitForFinished();
    }

    if (resizeWatcher->isRunning()){
        resizeWatcher->cancel();
        resizeWatcher->waitForFinished();
    }
}

bool binarycodepattern::interpolate()
{
    // Check if decoding finished
    if (!(maskResult.data && map_X.data && colorImage.data)) {
        emit reportInfo(QString("Error: No Decoding Data"));
        return false;
    }

    // Start Time Mesurement
    timer.start();

    // Start Interpolation
    interpolationData data = interpolationData(map_X, map_Y, maskResult, colorImage, 255, interpolationWindowSize,
                                               interpolationIterations, interpolationThreshold);
    interpolationWatcher->setFuture(QtConcurrent::run(interpolateMissingValues, data));

    // Report Start
    emit reportInfo(QString("Info: Interpolation started ..."));
    emit status(QString("Interpolation started"));
    return true;
}

bool binarycodepattern::resize()
{
    // Check if interpolation finished
    if (!(interpolatedMask.data && map_X_interpolated.data)) {
        emit reportInfo(QString("Error: No Interpolation Data"));
        return false;
    }

    // Start Time Mesurement
    timer.start();

    // Start resizing
    resizeData data = resizeData(map_X_interpolated, map_Y_interpolated, interpolatedMask,
                                 colorImage, 255, resizeFaktor, interpolationThreshold);
    resizeWatcher->setFuture(QtConcurrent::run(resizeResult, data));

    // Report Start
    emit reportInfo(QString("Info: Resizing started ..."));
    emit status(QString("Resizing started"));
    return true;
}

bool binarycodepattern::saveResult(QString url, bool ouputAllData)
{
    std::string url_string = url.toLocal8Bit().constData();
    if(url_string == "") {
        emit reportInfo(QString("Error: No output folder selected"));
        return false;
    }

    cv::Mat transparent;
    if (colorImage.data && ouputAllData){
        cv::imwrite(url_string + "/colorImage.png", colorImage );
        emit reportInfo(QString("Info: Color image saved"));
    }

    if (whiteImage.data && ouputAllData){
        cv::imwrite(url_string + "/whiteImage.png", whiteImage );
        emit reportInfo(QString("Info: White image saved"));
    }

    if (blackImage.data && ouputAllData){
        cv::imwrite(url_string + "/blackImage.png", blackImage );
        emit reportInfo(QString("Info: Black image saved"));
    }

    if (maskResult.data && ouputAllData){
        setValueTransparent(maskResult, transparent, maskResult, 255);
        cv::imwrite(url_string + "/mask.png", transparent );
        emit reportInfo(QString("Info: Mask saved"));
    }

    if (resultImage.data) {
        cv::imwrite(url_string + "/decoded.png", resultImage_t ); // Speichere Ergebnis (gemittelte Werte)
        emit reportInfo(QString("Info: Result saved"));
    }

    if (interpolatedImage.data) {
        cv::imwrite(url_string + "/interpolated.png", interpolatedImage ); // Speichere Ergebnis (gefilterte Werte)
        emit reportInfo(QString("Info: Interpolatet result saved"));
    }

    if (shadowMask.data && ouputAllData) {
        cv::imwrite(url_string + "/shadowMask.png", shadowMask ); // Speichere
        emit reportInfo(QString("Info: Shadow mask saved"));
    }

    if (resizedImage.data) {
        cv::imwrite(url_string + "/resized.png", resizedImage ); // Speichere resized result
        emit reportInfo(QString("Info: Resized result saved"));
    }

    if (ouputAllData){
        std::vector<cv::Mat> _patterns;
        binarycode::generatePatterns(this->beamerWidth, this->beamerHeight, _patterns);
        for (int i = 0; i < _patterns.size(); i++){
            std::stringstream ss;
            ss << std::setw(3) << std::setfill('0') << i;
            std::string s = ss.str();
            cv::imwrite(url_string + "/pattern_" + ss.str() + ".png", _patterns[i] );
        }
        emit reportInfo(QString("Info: Patterns saved"));
    }

    return true;
}

bool binarycodepattern::decode()
{
    // Stop Prozessing
    initFutureWatcher();

    // Test if Data loaded
    if (!isDataLoaded()){
        reportInfo(QString("Error: No Images loaded"));
        return false;
    }

    // Start time mesurement
    timer.start();

    // Computing shadows mask
    computeShadowMasks();

    // Crate Tiles
    QList<cv::Rect> tileList = splitImageInTiles(1000, imageWidth, imageHeigth);
    numberOfTiles = tileList.size();
    decodedTiles = 0;

    // Create Data object
    decodingInformation _info(captured_images,shadowMask,imageWidth,whiteThreshold,imageHeigth,
                              beamerWidth,beamerHeight,numOfRowImgs,numOfColImgs);

    // Function for decoding
    std::function<decodingResult(const cv::Rect&)> decodeTile = [ _info ](const cv::Rect &tile) {
        decodingResult _decodingResult;
        binarycode::decodePatterns(_info, tile, _decodingResult);
        return _decodingResult;
    };

    // Start decoding
    decodingWatcher->setFuture(QtConcurrent::mapped(tileList, decodeTile));

    // Report Start of Prozessing
    emit reportInfo(QString("Info: Decoding started with %1 tiles ... ").arg(tileList.size()));

    return true;
}

bool binarycodepattern::isDataLoaded()
{
    // Check if Pattern Images are loaded
    for (unsigned int i = 0; i < captured_images.size(); i++){
        if(!captured_images[i].data){
            emit reportInfo(QString("Pattern image %1 not loaded").arg(i));
            return false;
        }
    }

    // Check white image
    if (!isWhiteImageLoaded()) return false;
    if (!isBlackImageLoaded()) return false;
    if (!isColorImageLoaded()) return false;

    return true;
}

bool binarycodepattern::loadImages(QStringList urls)
{
    // If no extra color image is given
    if (urls.size() == numberOfPatternImages + 2){
        urls.push_back(urls.last()); // Add copy of last image url as color image
        emit reportInfo(QString("Info: No color image selected (--> black image will be used)"));
    }

    // Check if correct number of Images selected (patterns + white + black + color)
    if (urls.size() == numberOfPatternImages + 3) {

        // Create Vector for Images
        captured_images.clear();
        captured_images.resize( numberOfPatternImages );

        // Create vector with Struct of URL and color-flag (all images grayscale and last image color)
        QList<imageColorStruct> imageList;
        for (int i = 0; i < urls.size(); i++){
            // Init struct
            imageColorStruct image;

            // Set URL
            image.url = urls.at(i).toLocal8Bit().constData();

            // Set Color Flag
            image.colorFlag = cv::IMREAD_GRAYSCALE;
            if (i == numberOfPatternImages + 2) image.colorFlag = cv::IMREAD_COLOR;

            imageList.push_back(image);
        }

        // Function for loading
        std::function<cv::Mat(const imageColorStruct&)> loadImageToMat = [](const imageColorStruct &imageFileColor) {
            cv::Mat image_ = cv::imread(imageFileColor.url, imageFileColor.colorFlag);

            // Medianfilter
            //            cv::medianBlur(image_, image_, 3);

            return  image_;
        };

        // Use mapped to run the thread safe scale function on the files.
        imageLoadingWatcher->setFuture(QtConcurrent::mapped(imageList, loadImageToMat));
    }
    else {
        emit reportInfo(QString("Error: Wrong number of Images selected (%1/%2 [+1])").arg(urls.size()).arg(numberOfPatternImages + 2));
        return false;
    }
    emit reportInfo(QString("Info: Start loading images"));
    return true;
}

int binarycodepattern::setBlackThreshold(int threshold)
{
    if (threshold > -1 && threshold < 256) {
        blackThreshold = threshold;
        //        if (isDataLoaded()){
        //            computeShadowMasks();
        //            emit sendPreview(QPixmap::fromImage(QImage((unsigned char*) shadowMask.data, shadowMask.cols, shadowMask.rows, QImage::Format_Grayscale8)));
        //        }
    }
    else {
        emit reportInfo(QString("Please select value between 0 and 255"));
    }
    return blackThreshold;
}

int binarycodepattern::setWhiteThreshold(int threshold)
{
    if (threshold > -1 && threshold < 256) {
        whiteThreshold = threshold;
        //        if (isDataLoaded()){
        //            cv::Mat whiteThresholdImage;
        //            cv::absdiff(captured_images[5 * 2], captured_images[5 * 2 + 1], whiteThresholdImage);
        //            cv::threshold(whiteThresholdImage, whiteThresholdImage, double(whiteThreshold),255,cv::THRESH_BINARY);
        //            emit sendPreview(QPixmap::fromImage(QImage((unsigned char*)
        //                                                       whiteThresholdImage.data,
        //                                                       whiteThresholdImage.cols,
        //                                                       whiteThresholdImage.rows, QImage::Format_Grayscale8)));
        //        }
    }
    else {
        emit reportInfo(QString("Please select value between 0 and 255"));
    }
    return whiteThreshold;
}

int binarycodepattern::setInterpolationWindowSize(int windowSize)
{
    if ( windowSize > 2 ) {
        if (windowSize % 2 == 0) windowSize++; // Add 1 if even window size
        interpolationWindowSize = windowSize;
    }
    else {
        emit reportInfo(QString("Error: Please select odd value bigger than 3"));
    }
    return interpolationWindowSize;
}

double binarycodepattern::setInterpolationThreshold(double threshold)
{
    if ( threshold < 1 && threshold > 0 ) {
        interpolationThreshold = threshold;
    }
    else {
        emit reportInfo(QString("Error: Please select odd value bigger than 3"));
    }
    return interpolationThreshold;
}

int binarycodepattern::setInterpolationIterations(int itterations)
{
    if ( itterations > 0 ) {
        interpolationIterations = itterations;
    }
    else {
        emit reportInfo(QString("Error: Please select value bigger than 0"));
    }
    return interpolationIterations;
}

double binarycodepattern::setResizeFactor(double factor)
{
    if ( factor >= 1 ) {
        resizeFaktor = factor;
    }
    else {
        emit reportInfo(QString("Error: Please select value bigger or equal than 1"));
    }
    return resizeFaktor;
}


void binarycodepattern::imageLoadingSaveImage(int num)
{
    if (num < captured_images.size()){
        captured_images[num] = imageLoadingWatcher->resultAt(num);
        numberOfLoadetPatternImages++;
    }
    else if (num == captured_images.size()) {
        whiteImage = imageLoadingWatcher->resultAt(num);
        numberOfExtraImages++;
    }
    else if (num == captured_images.size() + 1) {
        blackImage = imageLoadingWatcher->resultAt(num);
        numberOfExtraImages++;
    }
    else if (num == captured_images.size() + 2) {
        colorImage = imageLoadingWatcher->resultAt(num);
        numberOfExtraImages++;
    }

    // Emit Signal for finished prozess
    emit dataLoadingUpdate();
    emit status(QString("Loading Images (%1%)").arg(100/(captured_images.size()+2)*(numberOfLoadetPatternImages + numberOfExtraImages)));
}

void binarycodepattern::imageLoadingFinished()
{
    // Deleating Data and Reinitialising
    imageLoadingWatcher->deleteLater();
    initFutureWatcher();

    if (!isDataLoaded()){
        // If Error while Loading clear all data
        captured_images.clear();
        blackImage = cv::Mat();
        whiteImage = cv::Mat();
        colorImage = cv::Mat();
        emit reportInfo(QString("Error: loading images"));
    }
    else {
        // Get Image Size
        imageHeigth = colorImage.size().height;
        imageWidth = colorImage.size().width;
        emit reportInfo(QString("Info: All images sucsessfully loaded"));
    }

    // Emit Signal for finished prozess
    emit dataLoadingUpdate();
    emit status(QString("Image loading finished"));
}

void binarycodepattern::decodingTileFinished()
{
    decodedTiles++;
    emit status(QString("Decoding (%1%)").arg(100/numberOfTiles*decodedTiles));
}

void binarycodepattern::decodingFinished()
{
    // Combine Results of decoding for all tiles
    cv::Mat anzahl = cv::Mat(beamerHeight, beamerWidth, CV_8UC1, cv::Scalar(0));
    cv::Mat map_X_summ = cv::Mat(beamerHeight, beamerWidth, CV_32FC1, cv::Scalar(0));
    cv::Mat map_Y_summ = cv::Mat(beamerHeight, beamerWidth, CV_32FC1, cv::Scalar(0));
    for (int tile = 0; tile < numberOfTiles; tile++) {
        anzahl += decodingWatcher->resultAt(tile).anzahl;
        map_X_summ += decodingWatcher->resultAt(tile).map_X_summ;
        map_Y_summ += decodingWatcher->resultAt(tile).map_Y_summ;
    }

    // Deleating Data and Reinitialising
    decodingWatcher->deleteLater();
    initFutureWatcher();

    // Create Average Maps
    cv::Mat anzahl_32;
    anzahl.convertTo(anzahl_32,CV_32FC1);
    map_X = map_X_summ/anzahl_32;
    map_Y = map_Y_summ/anzahl_32;
    cv::threshold(anzahl, maskResult, 0, 255, cv::THRESH_BINARY_INV); //?

    // Remap
    cv::remap(colorImage, resultImage, map_X, map_Y, cv::INTER_CUBIC);

    // Transparent
    setValueTransparent(resultImage, resultImage_t, maskResult, 255);

    // Stop time mesurement
    float runTimeMS = float(timer.elapsed())/1000;

    // Decoding abgeschlossen
    emit reportInfo(QString("Info: Decoding finished in %1 Seconds").arg(runTimeMS));
    emit status(QString("Decoding finished"));

    // Show preview
    emit sendPreview(QPixmap::fromImage(
                         QImage((unsigned char*) resultImage_t.data, resultImage_t.cols,
                                resultImage_t.rows, QImage::Format_RGBA8888).rgbSwapped()));
}

void binarycodepattern::interpolationFinished()
{
    // Get Data
    interpolationResult result = interpolationWatcher->result();
    map_X_interpolated = result._mapX;
    map_Y_interpolated = result._mapY;
    interpolatedImage = result._colorImage;
    interpolatedMask = result._mask;

    // Deleating Data and Reinitialising
    interpolationWatcher->deleteLater();
    initFutureWatcher();

    // Stop time mesurement
    float runTimeMS = float(timer.elapsed())/1000;
    emit reportInfo(QString("Info: Interpolation finished in %1 Seconds").arg(runTimeMS));
    emit status(QString("Interpolation finished"));

    // Show preview
    emit sendPreview(QPixmap::fromImage(
                         QImage((unsigned char*) interpolatedImage.data, interpolatedImage.cols,
                                interpolatedImage.rows, QImage::Format_RGBA8888).rgbSwapped()));

    resize();
}

void binarycodepattern::resizingFinished()
{
    resizedImage = resizeWatcher->result();

    // Deleating Data and Reinitialising
    resizeWatcher->deleteLater();
    initFutureWatcher();

    // Stop time mesurement
    float runTimeMS = float(timer.elapsed())/1000;
    emit reportInfo(QString("Info: Rezising finished in %1 Seconds").arg(runTimeMS));
    emit status(QString("Rezising finished"));
}

void binarycodepattern::computeShadowMasks()
{
    cv::absdiff(whiteImage, blackImage, shadowMask);
    cv::threshold(shadowMask, shadowMask, double(blackThreshold),255,cv::THRESH_BINARY);
}

void binarycodepattern::initFutureWatcher()
{
    // Future Watcher for Loading Images
    imageLoadingWatcher = new QFutureWatcher<cv::Mat>(this);
    connect(imageLoadingWatcher, &QFutureWatcher<cv::Mat>::resultReadyAt, this, &binarycodepattern::imageLoadingSaveImage);
    connect(imageLoadingWatcher, &QFutureWatcher<cv::Mat>::finished, this, &binarycodepattern::imageLoadingFinished);

    // Future Watcher for Decoding
    decodingWatcher = new QFutureWatcher<decodingResult>(this);
    connect(decodingWatcher, &QFutureWatcher<decodingResult>::resultReadyAt, this, &binarycodepattern::decodingTileFinished);
    connect(decodingWatcher, &QFutureWatcher<decodingResult>::finished, this, &binarycodepattern::decodingFinished);

    // Future Watcher for Interpolation
    interpolationWatcher = new QFutureWatcher<interpolationResult>(this);
    connect(interpolationWatcher, &QFutureWatcher<interpolationResult>::finished, this, &binarycodepattern::interpolationFinished);

    // Future Watcher for Resizing
    resizeWatcher = new QFutureWatcher<cv::Mat>(this);
    connect(resizeWatcher, &QFutureWatcher<cv::Mat>::finished, this, &binarycodepattern::resizingFinished);
}

void binarycodepattern::clearData()
{
    // Clear Data
    resultImage.release();
    maskResult.release();
    interpolatedImage.release();
    map_X.release();
    map_Y.release();
    map_X_interpolated.release();
    map_Y_interpolated.release();
    shadowMask.release();
    captured_images.clear();
    whiteImage.release();
    blackImage.release();
    colorImage.release();
}
