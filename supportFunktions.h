#pragma once
#include <iostream> 
#include <numeric>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <QList>


struct imageColorStruct
{
    std::string url;
    int colorFlag;
};

struct decodingResult
{
    cv::Mat anzahl;
    cv::Mat map_X_summ;
    cv::Mat map_Y_summ;
};

struct decodingInformation
{
    decodingInformation(std::vector<cv::Mat> &captured_images, cv::Mat &shadowMask, int &imageWidth, int &whiteThreshold,
                        int &imageHeigth, int &beamerWidth, int &beamerHeight, size_t &numOfRowImgs, size_t &numOfColImgs){
        _shadowMask = shadowMask;
        _captured_images = captured_images;
        _imageHeigth = imageHeigth;
        _imageWidth = imageWidth;
        _whiteThreshold = whiteThreshold;
        _beamerHeight = beamerHeight;
        _beamerWidth = beamerWidth;
        _numOfColImgs = numOfColImgs;
        _numOfRowImgs = numOfRowImgs;
    }
    cv::Mat _shadowMask;
    std::vector<cv::Mat> _captured_images;
    int _imageWidth, _imageHeigth, _beamerWidth, _beamerHeight, _whiteThreshold;
    size_t _numOfRowImgs, _numOfColImgs;
};


struct interpolationData
{
    interpolationData(cv::Mat mapX, cv::Mat mapY, cv::Mat mask, cv::Mat colorImage, int nanValue, int windowSize, int itterations, double threshold) {
        _mapX = mapX;
        _mapY = mapY;
        _mask = mask;
        _colorImage = colorImage;
        _windowSize = windowSize;
        _itterations = itterations;
        _threshold = threshold;
        _nanValue = nanValue;

    }
    cv::Mat _mapX, _mapY, _mask, _colorImage;
    int _nanValue, _windowSize, _itterations;
    double _threshold;
};

struct interpolationResult
{
    interpolationResult(){}
    interpolationResult(cv::Mat mapX, cv::Mat mapY, cv::Mat mask, cv::Mat colorImage) {
        _mapX = mapX;
        _mapY = mapY;
        _mask = mask;
        _colorImage = colorImage;
    }
    cv::Mat _mapX, _mapY, _mask, _colorImage;
};

struct resizeData
{
    resizeData(cv::Mat mapX, cv::Mat mapY, cv::Mat mask, cv::Mat colorImage, int nanValue, double resizeFactor, double threshold) {
        _mapX = mapX;
        _mapY = mapY;
        _mask = mask;
        _colorImage = colorImage;
        _threshold = threshold;
        _nanValue = nanValue;
        _resizeFactor = resizeFactor;
    }
    cv::Mat _mapX, _mapY, _mask, _colorImage;
    int _nanValue;
    double _threshold, _resizeFactor;
};


float median(std::vector<float> &v);
float mean(std::vector<float> &v);
int setValueTransparent(cv::Mat &imageIn, cv::Mat &imageOut, cv::Mat & mask, int nanValue);
void interpolateMissingValues(cv::Mat &imageIn, cv::Mat &imageOut, cv::Mat &mask, int nanValue,
                                    int windowSize, double theshold, int itterations, cv::Mat &_newMask);
interpolationResult interpolateMissingValues(interpolationData data);
cv::Mat resizeResult(resizeData data);
QList<cv::Rect> splitImageInTiles(int tileSize, int width, int heigth);
