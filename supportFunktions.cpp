#include "supportFunktions.h"



float median(std::vector<float>& v)
{
    size_t n = v.size() / 2;
    nth_element(v.begin(), v.begin() + n, v.end());
    return v[n];
}

float mean(std::vector<float>& v)
{
    return std::accumulate(v.begin(), v.end(), 0.0) / v.size();
}


int setValueTransparent(cv::Mat & imageIn, cv::Mat & imageOut, cv::Mat & mask, int nanValue)
{
    // new RGBA-Image
    imageOut = cv::Mat(imageIn.size().height, imageIn.size().width, CV_8UC4, cv::Scalar(0, 0, 0, 0));


    for (int i = 0; i < imageIn.size().height; i++)
    {
        for (int j = 0; j < imageIn.size().width; j++)
        {
            if (mask.at<uchar>(i, j) == nanValue) continue;

            // RGB
            uchar a = 0, b = 0, c = 0;
            if (imageIn.type() == CV_8UC3){
                a =  imageIn.at<cv::Vec3b>(i, j)[0];
                b = imageIn.at<cv::Vec3b>(i, j)[1];
                c = imageIn.at<cv::Vec3b>(i, j)[2];
            }

            imageOut.at<cv::Vec4b>(i, j) = cv::Vec4b(a,b,c,255);
        }
    }
    return 0;
}


void interpolateMissingValues(cv::Mat & imageIn, cv::Mat & imageOut, cv::Mat &mask, int nanValue, int windowSize,
                              double theshold, int itterations, cv::Mat &_newMask)
{
    int radius = (windowSize - 1) / 2;
    imageOut = imageIn.clone();
    _newMask = mask.clone();

    for (int anzahl = 1; anzahl < itterations +1; anzahl++)
    {
        for (int i = 1 + radius; i < (imageOut.size().height - radius - 1); i++)
        {
            for (int j = 1 + radius; j < (imageOut.size().width - radius - 1); j++)
            {
                if (_newMask.at<uchar>(i, j) != nanValue) continue;

                // BBox
                cv::Rect bBox = cv::Rect(j - radius, i - radius, windowSize, windowSize);

                // copy window
                cv::Mat ausschnitt, ausch_mask;
                imageOut(bBox).copyTo(ausschnitt);
                _newMask(bBox).copyTo(ausch_mask);

                // serach window
                std::vector<float> werte;
                for (int x = 0; x < ausschnitt.size().width; x++) {
                    for (int y = 0; y < ausschnitt.size().height; y++) {
                        if (ausch_mask.at<uchar>(x, y) == nanValue) continue;
                        werte.push_back(ausschnitt.at<float>(x, y));
                    }
                }

                // check if number of pixel over threshold
                if (werte.size() < windowSize*windowSize*theshold) continue;

                // Median
                //imageOut.at<float>(i, j) = median(werte);

                // Mean
                imageOut.at<float>(i, j) = mean(werte);
                _newMask.at<uchar>(i, j) = 0;
            }
        }
    }
}


QList<cv::Rect> splitImageInTiles(int tileSize, int width, int heigth)
{
    QList<cv::Rect>tileList;

    for (int w = 0; w < width; w += tileSize){
        for (int h = 0; h < heigth; h += tileSize){
            int tileWidth = (w+tileSize > width) ? width-w : tileSize;
            int tileHeigth = (h+tileSize > heigth) ? heigth-h : tileSize;

            tileList.push_back(cv::Rect(w,h,tileWidth,tileHeigth));
        }
    }

    return tileList;
}

interpolationResult interpolateMissingValues(interpolationData data)
{
    interpolationResult result = interpolationResult();

    // Interpolating Missing Values using the mask
    interpolateMissingValues(data._mapX, result._mapX, data._mask, data._nanValue,
                             data._windowSize, data._threshold, data._itterations, result._mask);
    interpolateMissingValues(data._mapY, result._mapY, data._mask, data._nanValue,
                             data._windowSize, data._threshold, data._itterations, result._mask);

    /*
    // bilateral filter filter --> wrong edges
    cv::Mat t_mapX, t_mapY;
    cv::bilateralFilter(result._mapX, t_mapX, 6, 50, 50);
    cv::bilateralFilter(result._mapY, t_mapY, 6, 50, 50);
    result._mapX = t_mapX;
    result._mapY = t_mapY;
*/

    // Median
    cv::medianBlur(result._mapX, result._mapX, 3);
    cv::medianBlur(result._mapY, result._mapY, 3);

    // Remap Result
    cv::Mat _result_filterd;
    cv::remap(data._colorImage, _result_filterd, result._mapX, result._mapY, cv::INTER_CUBIC);
    setValueTransparent(_result_filterd,result._colorImage,result._mask,data._nanValue);

    return result;
}

cv::Mat resizeResult(resizeData data)
{
    cv::Mat mapX_r, mapY_r, mask_r, _resizedResult, resizedImage;
    cv::resize(data._mapX, mapX_r, cv::Size(),data._resizeFactor, data._resizeFactor, cv::INTER_LINEAR);
    cv::resize(data._mapY, mapY_r, cv::Size(),data._resizeFactor, data._resizeFactor, cv::INTER_LINEAR);
    cv::resize(data._mask, mask_r, cv::Size(),data._resizeFactor, data._resizeFactor, cv::INTER_NEAREST);
    cv::morphologyEx(mask_r,mask_r,cv::MORPH_DILATE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3,3)));

    cv::remap(data._colorImage, _resizedResult, mapX_r, mapY_r, cv::INTER_CUBIC);
    setValueTransparent(_resizedResult, resizedImage, mask_r, data._nanValue);

    return resizedImage;
}
