#include "patternwindow.h"

PatternWindow::PatternWindow(QRect _screen)
{
    // Set Screen
    screen = _screen;

    // Set up Layout
    viewLayout = new QGridLayout;
    viewLayout->setSpacing(0);
    viewLayout->setMargin(0);
    viewLayout->setContentsMargins(0,0,0,0);
    imageLabel = new QLabel();
    viewLayout->addWidget(imageLabel);
    this->setLayout(viewLayout);

    // Move to desirerd Screen
    this->move(screen.center());

    // Set up Timer
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(nextPattern()));
    timerPaused = false;

    // Generate Patterns
    binarycode::generatePatterns(screen.width(), screen.height(), generatetPattern);

    this->show();
    this->showFullScreen();

}

PatternWindow::~PatternWindow()
{

}

void PatternWindow::setTimer(int time_ms)
{
    waitTime = time_ms;
}

int PatternWindow::getNumberOfPatterns()
{
    return int(generatetPattern.size());
}

QRect PatternWindow::getScreen()
{
    return screen;
}

void PatternWindow::start()
{
currentPatternID = 0;
                    updatePattern();
                    timer->start(waitTime);
}

void PatternWindow::stop()
{
    timer->stop();
    this->hide();
    currentPatternID = 0;
    emit isStopt();
}

void PatternWindow::pause()
{
    if (timerPaused){
        timer->start(waitTime);
        timerPaused = false;
    }
    else
    {
        timer->stop();
        timerPaused = true;
    }
}

void PatternWindow::nextPattern()
{
    currentPatternID++;
    updatePattern();
}

void PatternWindow::previousPattern()
{
    if (currentPatternID > 0)
        currentPatternID--;

    updatePattern();
}

void PatternWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Escape)
        stop();

    if(event->key() == Qt::Key_Space)
        pause();

    if (event->key() == Qt::Key_Right)
        nextPattern();

    if (event->key() == Qt::Key_Left)
        previousPattern();
}

void PatternWindow::updatePattern()
{
    // Check currend Pattern ID
    if (currentPatternID > -1 && currentPatternID < generatetPattern.size()) {
        // Send to Window and show
        imageLabel->setPixmap(generatetPattern[currentPatternID]);

        emit currentPattern(currentPatternID+1);
    }
    else
        this->stop();
}
